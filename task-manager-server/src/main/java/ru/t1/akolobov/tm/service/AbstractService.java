package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IService;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.IndexIncorrectException;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull Connection connection = connectionService.getConnection();
        M newModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            newModel = repository.add(model);
            connection.commit();
            return newModel;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M update(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull Connection connection = connectionService.getConnection();
        M newModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            newModel = repository.update(model);
            connection.commit();
            return newModel;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull Connection connection = connectionService.getConnection();
        List<M> newModels;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            newModels = new ArrayList<>(repository.add(models));
            connection.commit();
            return newModels;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull Connection connection = connectionService.getConnection();
        List<M> newModels;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            newModels = new ArrayList<>(repository.set(models));
            connection.commit();
            return newModels;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(sort);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }


    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return Optional.ofNullable(repository.findOneByIndex(index))
                    .orElseThrow(EntityNotFoundException::new);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Integer getSize() {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.remove(model);
            connection.commit();
            return model;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public int removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            int rowsModified = repository.removeById(id);
            connection.commit();
            return rowsModified;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            M model = repository.removeByIndex(index);
            connection.commit();
            return model;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
