package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.endpoint.IUserEndpoint;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.marker.IntegrationCategory;
import ru.t1.akolobov.tm.model.User;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.Date;

import static ru.t1.akolobov.tm.data.TestUser.NEW_USER;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserEndpointTest {

    public static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance("localhost", "6060");
    public static IUserEndpoint userEndpoint = IUserEndpoint.newInstance("localhost", "6060");

    public static String adminToken;
    public static String userToken;

    @BeforeClass
    public static void prepareSession() {
        adminToken = authEndpoint.login(new UserLoginRequest("akolobov", "akolobov")).getToken();
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
    }

    @AfterClass
    public static void closeSessions() {
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        adminToken = null;
        userToken = null;
    }

    @Test
    public void changePassword() {
        @NotNull UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(userToken);
        changePasswordRequest.setPassword("NEW_PASSWORD");
        userEndpoint.changePassword(changePasswordRequest);
        Assert.assertNull(authEndpoint
                .login(new UserLoginRequest("user1", "user1"))
                .getToken()
        );
        userToken = authEndpoint
                .login(new UserLoginRequest("user1", "NEW_PASSWORD"))
                .getToken();
        Assert.assertNotNull(userToken);
        changePasswordRequest.setPassword("user1");
        userEndpoint.changePassword(changePasswordRequest);
    }

    @Test
    public void lockUser() {
        @NotNull UserLockRequest lockRequest = new UserLockRequest(userToken);
        lockRequest.setLogin("user1");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> userEndpoint.lockUser(lockRequest)
        );
        lockRequest.setToken(adminToken);
        userEndpoint.lockUser(lockRequest);
        authEndpoint.logout(new UserLogoutRequest(userToken));
        Assert.assertNull(authEndpoint
                .login(new UserLoginRequest("user1", "user1"))
                .getToken()
        );

        @NotNull UserUnlockRequest unlockRequest = new UserUnlockRequest(adminToken);
        unlockRequest.setLogin("user1");
        userEndpoint.unlockUser(unlockRequest);
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
    }

    @Test
    public void registryUser() {
        @NotNull UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(NEW_USER.getLogin());
        registryRequest.setPassword(NEW_USER.getLogin());
        registryRequest.setEmail(NEW_USER.getEmail());
        userEndpoint.registryUser(registryRequest);
        String newUserToken = authEndpoint
                .login(new UserLoginRequest(NEW_USER.getLogin(), NEW_USER.getLogin()))
                .getToken();
        Assert.assertNotNull(newUserToken);
        authEndpoint.logout(new UserLogoutRequest(newUserToken));
    }

    @Test
    public void removeUser() {
        @NotNull UserRemoveRequest removeRequest = new UserRemoveRequest(userToken);
        removeRequest.setLogin(NEW_USER.getLogin());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> userEndpoint.removeUser(removeRequest)
        );
        removeRequest.setToken(adminToken);
        userEndpoint.removeUser(removeRequest);
        Assert.assertNull(authEndpoint
                .login(new UserLoginRequest(NEW_USER.getLogin(), NEW_USER.getLogin()))
                .getToken()
        );
    }

    @Test
    public void unlockUser() {
        @NotNull UserLockRequest lockRequest = new UserLockRequest(adminToken);
        lockRequest.setLogin("user1");
        userEndpoint.lockUser(lockRequest);
        Assert.assertNull(authEndpoint
                .login(new UserLoginRequest("user1", "user1"))
                .getToken()
        );

        @NotNull UserUnlockRequest unlockRequest = new UserUnlockRequest(userToken);
        unlockRequest.setLogin("user1");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> userEndpoint.unlockUser(unlockRequest)
        );
        unlockRequest.setToken(adminToken);
        userEndpoint.unlockUser(unlockRequest);
        authEndpoint.logout(new UserLogoutRequest(userToken));
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
    }

    @Test
    public void updateProfile() {
        @NotNull UserUpdateProfileRequest updateProfileRequest = new UserUpdateProfileRequest(userToken);
        @NotNull String firstName = "FirstName" + new Date().getTime();
        @NotNull String lastName = "LastName" + new Date().getTime();
        @NotNull String middleName = "MiddleName" + new Date().getTime();
        updateProfileRequest.setFirstName(firstName);
        updateProfileRequest.setLastName(lastName);
        updateProfileRequest.setMiddleName(middleName);
        User user = userEndpoint.updateProfile(updateProfileRequest).getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> userEndpoint.updateProfile(new UserUpdateProfileRequest())
        );
    }

}
