package ru.t1.akolobov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    M update(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    boolean existById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @NotNull
    Integer getSize();

    @NotNull
    M remove(@NotNull M model);

    int removeById(@NotNull String id);

    @NotNull
    M removeByIndex(@NotNull Integer index);

}
