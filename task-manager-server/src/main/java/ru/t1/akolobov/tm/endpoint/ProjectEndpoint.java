package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.api.service.IProjectTaskService;
import ru.t1.akolobov.tm.api.service.IServiceLocator;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = getServiceLocator().getProjectService();
        this.projectTaskService = getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                request.getStatus()
        );
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusByIndex(
                userId,
                request.getIndex(),
                request.getStatus()
        );
        return new ProjectChangeStatusByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                Status.COMPLETED
        );
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusByIndex(
                userId,
                request.getIndex(),
                Status.COMPLETED
        );
        return new ProjectCompleteByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse create(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectCreateResponse(
                projectService.create(
                        userId,
                        request.getName(),
                        request.getDescription()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse getById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectGetByIdResponse(
                projectService.findOneById(userId, request.getId())
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIndexResponse getByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectGetByIndexResponse(
                projectService.findOneByIndex(
                        userId,
                        request.getIndex()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse list(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        return new ProjectListResponse(
                projectService.findAll(
                        userId,
                        request.getSortType()
                )
        );
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectTaskService.removeProjectById(userId, request.getId());
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        @Nullable final Project project = projectService.findOneByIndex(userId, request.getIndex());
        if (project == null) throw new ProjectNotFoundException();
        projectTaskService.removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusById(
                userId,
                request.getId(),
                Status.IN_PROGRESS
        );
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIndexResponse startByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.changeStatusByIndex(
                userId,
                request.getIndex(),
                Status.IN_PROGRESS
        );
        return new ProjectStartByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.updateById(
                userId,
                request.getId(),
                request.getName(),
                request.getDescription()
        );
        return new ProjectUpdateByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @NotNull final String userId = check(request).getUserId();
        projectService.updateByIndex(
                userId,
                request.getIndex(),
                request.getName(),
                request.getDescription()
        );
        return new ProjectUpdateByIndexResponse();
    }

}
