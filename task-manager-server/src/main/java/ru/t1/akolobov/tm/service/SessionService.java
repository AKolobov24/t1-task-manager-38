package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.ISessionService;
import ru.t1.akolobov.tm.model.Session;
import ru.t1.akolobov.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session>
        implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
