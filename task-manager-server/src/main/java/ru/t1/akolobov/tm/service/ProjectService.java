package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.*;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project>
        implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            repository.update(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setStatus(status);
            repository.update(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return add(userId, new Project(name));
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return add(userId, new Project(name));
        return add(userId, new Project(name, description));
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return add(userId, new Project(name));
        return add(userId, new Project(name, status));
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            @NotNull final Project project = Optional
                    .ofNullable(repository.findOneByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new);
            project.setName(name);
            project.setDescription(description);
            repository.update(userId, project);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}