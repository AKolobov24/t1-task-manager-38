package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId,
                       @Nullable Integer index,
                       @Nullable String name,
                       @Nullable String description);

}
