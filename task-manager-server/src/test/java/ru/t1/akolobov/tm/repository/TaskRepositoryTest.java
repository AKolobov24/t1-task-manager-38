package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.model.Task;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    private static Connection connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        connection.setAutoCommit(true);
        @NotNull IUserRepository userRepository = new UserRepository(connection);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        @NotNull IUserRepository userRepository = new UserRepository(connection);
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void clearData() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        @NotNull Task task = createTask();
        repository.add(USER1_ID, task);
        task.setUserId(USER1_ID);
        Assert.assertEquals(task, repository.findAll(USER1_ID).get(0));
        repository.add(USER_EMPTY_ID, task);
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        List<Task> TaskList = createTaskList(USER1_ID);
        repository.add(TaskList);
        int size = repository.getSize();
        repository.clear(USER1_ID);
        Assert.assertEquals(
                size - TaskList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        @NotNull String taskId = repository.add(createTask(USER1_ID)).getId();
        Assert.assertTrue(repository.existById(USER1_ID, taskId));
        Assert.assertFalse(repository.existById(USER2_ID, taskId));
    }

    @Test
    public void findAll() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        List<Task> user1TaskList = createTaskList(USER1_ID);
        List<Task> user2TaskList = createTaskList(USER2_ID);
        repository.add(user1TaskList);
        repository.add(user2TaskList);
        Assert.assertEquals(user1TaskList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        @NotNull Task task = repository.add(createTask(USER1_ID));
        @NotNull String taskId = task.getId();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, taskId));
        Assert.assertNull(repository.findOneById(USER2_ID, taskId));
    }

    @Test
    public void findOneByIndex() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        int lastIndex = repository.getSize(USER1_ID) - 1;
        Assert.assertEquals(task, repository.findOneByIndex(USER1_ID, lastIndex));
        Assert.assertNull(repository.findOneByIndex(USER1_ID, lastIndex + 1));
    }

    @Test
    public void getSize() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        List<Task> taskList = createTaskList(USER1_ID);
        repository.add(taskList);
        Assert.assertEquals((Integer) taskList.size(), repository.getSize(USER1_ID));
        repository.add(createTask(USER1_ID));
        Assert.assertEquals((Integer) (taskList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        Assert.assertNull(repository.remove(USER_EMPTY_ID, task));
        Assert.assertEquals(task, repository.findOneById(USER1_ID, task.getId()));
        repository.remove(USER1_ID, task);
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void removeById() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        repository.add(createTaskList(USER1_ID));
        @NotNull Task task = repository.add(createTask(USER1_ID));
        repository.removeById(USER1_ID, task.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
        Assert.assertEquals(0, repository.removeById(USER2_ID, task.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        List<Task> taskList = createTaskList(USER1_ID);
        repository.add(taskList);
        int indexForCheck = repository.getSize(USER1_ID) - 1;
        Task task = repository.findOneByIndex(USER1_ID, indexForCheck);
        Assert.assertNotNull(task);
        Assert.assertEquals(task, repository.removeByIndex(USER1_ID, indexForCheck));
        Assert.assertEquals(indexForCheck, repository.getSize(USER1_ID).intValue());
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
        repository.removeByIndex(USER1_ID, taskList.size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull ITaskRepository repository = new TaskRepository(connection);
        @NotNull IProjectRepository projectRepository = new ProjectRepository(connection);
        Project project = projectRepository.add(USER1_ID, new Project("test-project-for-task"));
        Assert.assertNotNull(project);
        String projectId = project.getId();
        List<Task> taskList = createTaskList(USER1_ID);
        taskList.forEach(t -> t.setProjectId(projectId));
        repository.add(taskList);
        repository.add(createTask(USER1_ID));
        Assert.assertEquals(taskList, repository.findAllByProjectId(USER1_ID, projectId));
        taskList.forEach(t -> repository.remove(USER1_ID, t));
        projectRepository.removeById(USER1_ID, projectId);
    }

}
