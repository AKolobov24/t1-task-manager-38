package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static Task createTask() {
        return new Task("new-task", "new-task-desc");
    }

    @NotNull
    public static List<Task> createTaskList(int size) {
        @NotNull List<Task> taskList = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            @NotNull Task task = new Task("task-" + i, "task-" + i + "desc");
            taskList.add(task);
        }
        return taskList;
    }


}
