package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.Project;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    private static Connection connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        connection.setAutoCommit(true);
        @NotNull IUserRepository userRepository = new UserRepository(connection);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        @NotNull IUserRepository userRepository = new UserRepository(connection);
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void clearData() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        @NotNull Project project = createProject();
        repository.add(USER1_ID, project);
        project.setUserId(USER1_ID);
        Assert.assertTrue(repository.findAll(USER1_ID).contains(project));
        repository.add(USER_EMPTY_ID, project);
        Assert.assertEquals(1, repository.findAll(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        int size = repository.getSize();
        repository.clear(USER1_ID);
        Assert.assertEquals(
                size - projectList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        @NotNull String projectId = repository.add(createProject(USER1_ID)).getId();
        Assert.assertTrue(repository.existById(USER1_ID, projectId));
        Assert.assertFalse(repository.existById(USER2_ID, projectId));
    }

    @Test
    public void findAll() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        List<Project> user1ProjectList = createProjectList(USER1_ID);
        List<Project> user2ProjectList = createProjectList(USER2_ID);
        repository.add(user1ProjectList);
        repository.add(user2ProjectList);
        Assert.assertEquals(user1ProjectList, repository.findAll(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAll(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        @NotNull Project project = repository.add(createProject(USER1_ID));
        @NotNull String projectId = project.getId();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, projectId));
        Assert.assertNull(repository.findOneById(USER2_ID, projectId));
    }

    @Test
    public void findOneByIndex() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        repository.add(createProjectList(USER1_ID));
        int lastIndex = repository.getSize(USER1_ID) - 1;
        @NotNull Project project = repository.add(createProject(USER1_ID));
        Assert.assertEquals(project, repository.findOneByIndex(USER1_ID, lastIndex + 1));
        Assert.assertNull(repository.findOneByIndex(USER1_ID, lastIndex + 2));
    }

    @Test
    public void getSize() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        Assert.assertEquals((Integer) projectList.size(), repository.getSize(USER1_ID));
        repository.add(createProject(USER1_ID));
        Assert.assertEquals((Integer) (projectList.size() + 1), repository.getSize(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        repository.add(createProjectList(USER1_ID));
        @NotNull Project project = repository.add(createProject(USER1_ID));
        Assert.assertNull(repository.remove(USER_EMPTY_ID, project));
        Assert.assertEquals(project, repository.findOneById(USER1_ID, project.getId()));
        repository.remove(USER1_ID, project);
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

    @Test
    public void removeById() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        repository.add(createProjectList(USER1_ID));
        @NotNull Project project = repository.add(createProject(USER1_ID));
        repository.removeById(USER1_ID, project.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
        Assert.assertEquals(0, repository.removeById(USER2_ID, project.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        List<Project> projectList = createProjectList(USER1_ID);
        repository.add(projectList);
        int indexForCheck = repository.getSize(USER1_ID) - 1;
        Project project = repository.findOneByIndex(USER1_ID, indexForCheck);
        Assert.assertNotNull(project);
        Assert.assertEquals(project, repository.removeByIndex(USER1_ID, indexForCheck));
        Assert.assertEquals(indexForCheck, repository.getSize(USER1_ID).intValue());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
        repository.removeByIndex(USER1_ID, projectList.size());
    }

}
