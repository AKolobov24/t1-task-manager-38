package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @Nullable
    M update(@Nullable String userId, @Nullable M model);

    void clear(@Nullable String userId);

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Integer getSize(@Nullable String userId);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    int removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

}
