package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IUserOwnedRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    protected final String USER_ID_COLUMN = "user_id";

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return null;
        model.setUserId(userId);
        add(model);
        return model;
    }

    @Override
    @Nullable
    public M update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return null;
        model.setUserId(userId);
        update(model);
        return model;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }


    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull List<M> models = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) models.add(fetch(rowSet));
            rowSet.close();
        }
        return models;
    }


    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @NotNull Sort sort) {
        @NotNull List<M> models = new ArrayList<>();
        @NotNull String sql = String.format(
                "SELECT * FROM %s WHERE user_id = ? ORDER BY %s",
                getTableName(),
                getSortColumn(sort)
        );
        try (@NotNull PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) models.add(fetch(rowSet));
            rowSet.close();
        }
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        M model = null;
        @NotNull String sql = String.format("SELECT * FROM %s WHERE user_id = ? AND id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) model = fetch(rowSet);
            rowSet.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex (@NotNull final String userId, @NotNull final Integer index) {
        M model = null;
        @NotNull String sql = String.format("SELECT * FROM %s WHERE user_id = ? LIMIT 1 OFFSET ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) model = fetch(rowSet);
            rowSet.close();
        }
        return model;
    }


    @NotNull
    @Override
    @SneakyThrows
    public Integer getSize(@NotNull final String userId) {
        int size = 0;
        @NotNull String sql = String.format("SELECT count(*) as size FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (rowSet.next())
                size = rowSet.getInt("size");
            rowSet.close();
        }
        return size;
    }

    @Nullable
    @Override
    public M remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return null;
        removeById(userId, model.getId());
        return model;
    }

    private void removeAll(final @NotNull List<M> models) {
        if (models.isEmpty()) return;
        models.forEach(this::remove);
    }

    @Override
    @SneakyThrows
    public int removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull String sql = String.format("DELETE FROM %s WHERE user_id = ? AND id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            return statement.executeUpdate();
        }
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return remove(Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(EntityNotFoundException::new));
    }

}
