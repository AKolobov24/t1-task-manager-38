package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.model.User;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    private static Connection connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        connection.setAutoCommit(true);
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    @Test
    public void add() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(USER1);
        Assert.assertTrue(repository.findAll().contains(USER1));
        repository.remove(USER1);
    }

    @Test
    @Ignore
    public void clear() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        List<User> userList = createUserList();
        repository.add(userList);
        Assert.assertEquals(userList.size(), repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void existById() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        @NotNull String userId = repository.add(USER1).getId();
        Assert.assertTrue(repository.existById(userId));
        Assert.assertFalse(repository.existById(USER2.getId()));
        repository.remove(USER1);
    }

    @Test
    public void findAll() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        List<User> user1UserList = createUserList();
        repository.add(user1UserList);
        Assert.assertTrue(repository.findAll().containsAll(user1UserList));
        user1UserList.forEach(repository::remove);
    }

    @Test
    public void findOneById() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findOneById(USER1.getId()));
        Assert.assertNull(repository.findOneById(USER2.getId()));
        repository.remove(USER1);
    }

    @Test
    @Ignore
    public void findOneByIndex() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        List<User> userList = createUserList();
        repository.add(userList);
        int lastIndex = repository.getSize() - 1;
        repository.add(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findOneByIndex(lastIndex + 1));
        Assert.assertNull(repository.findOneByIndex(lastIndex + 2));
        repository.remove(NEW_USER);
        userList.forEach(repository::remove);
    }

    @Test
    public void getSize() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        List<User> userList = repository.findAll();
        Assert.assertEquals(userList.size(), repository.getSize().intValue());
        repository.add(NEW_USER);
        Assert.assertEquals((userList.size() + 1), repository.getSize().intValue());
        repository.remove(NEW_USER);
    }

    @Test
    public void remove() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(NEW_USER);
        Assert.assertEquals(NEW_USER, repository.findOneById(NEW_USER.getId()));
        Integer size = repository.getSize();
        repository.remove(NEW_USER);
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
    }

    @Test
    public void removeById() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(NEW_USER);
        Assert.assertNotNull(repository.findOneById(NEW_USER.getId()));
        repository.removeById(NEW_USER.getId());
        Assert.assertNull(repository.findOneById(NEW_USER.getId()));
        Assert.assertEquals(0, repository.removeById(NEW_USER.getId()));
    }

    @Ignore
    @Test(expected = EntityNotFoundException.class)
    public void removeByIndex() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        List<User> userList = createUserList();
        repository.add(userList);
        int size = repository.getSize();
        int indexForCheck = size - 1;
        User user = repository.findOneByIndex(indexForCheck);
        Assert.assertNotNull(user);
        Assert.assertEquals(user, repository.removeByIndex(indexForCheck));
        Assert.assertEquals(size - 1, repository.getSize().intValue());
        Assert.assertNull(repository.findOneById(user.getId()));
        userList.forEach(repository::remove);
        repository.removeByIndex(size);
    }

    @Test
    public void findByLogin() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByLogin(USER1.getLogin()));
        Assert.assertNull(repository.findOneById(USER2.getLogin()));
        repository.remove(USER1);
    }

    @Test
    public void findByEmail() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertEquals(USER1, repository.findByEmail(USER1.getEmail()));
        Assert.assertNull(repository.findByEmail("user_2@email.ru"));
        repository.remove(USER1);
    }

    @Test
    public void isLoginExist() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        repository.add(USER1);
        Assert.assertTrue(repository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(repository.isLoginExist(USER2.getLogin()));
        repository.remove(USER1);
    }

    @Test
    public void isEmailExist() {
        @NotNull IUserRepository repository = new UserRepository(connection);
        if (USER1.getEmail() == null) USER1.setEmail("user_1@email.ru");
        repository.add(USER1);
        Assert.assertTrue(repository.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(repository.isEmailExist("user_2@email.ru"));
        repository.remove(USER1);
    }

}
