package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String TABLE_NAME = "tm_user";

    @NotNull
    private final String LOGIN_COLUMN = "login";

    @NotNull
    private final String PASSWORD_HASH_COLUMN = "password_hash";

    @NotNull
    private final String ROLE_COLUMN = "role";

    @NotNull
    private final String LOCKED_COLUMN = "locked";

    @NotNull
    private final String EMAIL_COLUMN = "email";

    @NotNull
    private final String FIRST_NAME_COLUMN = "first_name";

    @NotNull
    private final String LAST_NAME_COLUMN = "last_name";

    @NotNull
    private final String MIDDLE_NAME_COLUMN = "middle_name";

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @SneakyThrows
    public void checkDatabaseTable() {
        @NotNull final String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s " +
                        "( " +
                        ID_COLUMN + " character(36) PRIMARY KEY," +
                        CREATED_COLUMN + " timestamp," +
                        LOGIN_COLUMN + " varchar(255)," +
                        PASSWORD_HASH_COLUMN + " varchar(255)," +
                        ROLE_COLUMN + " varchar(255)," +
                        LOCKED_COLUMN + " boolean," +
                        EMAIL_COLUMN + " varchar(255)," +
                        FIRST_NAME_COLUMN + " varchar(255)," +
                        LAST_NAME_COLUMN + " varchar(255)," +
                        MIDDLE_NAME_COLUMN + " varchar(255)" +
                        ")",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull ResultSet rowSet) {
        @NotNull User user = new User();
        user.setId(rowSet.getString(ID_COLUMN));
        user.setCreated(rowSet.getDate(CREATED_COLUMN));
        user.setLogin(rowSet.getString(LOGIN_COLUMN));
        user.setPasswordHash(rowSet.getString(PASSWORD_HASH_COLUMN));
        user.setRole(Optional
                .ofNullable(Role.toRole(rowSet.getString(ROLE_COLUMN)))
                .orElse(Role.USUAL)
        );
        user.setLocked(rowSet.getBoolean(LOCKED_COLUMN));
        user.setEmail(rowSet.getString(EMAIL_COLUMN));
        user.setFirstName(rowSet.getString(FIRST_NAME_COLUMN));
        user.setLastName(rowSet.getString(LAST_NAME_COLUMN));
        user.setMiddleName(rowSet.getString(MIDDLE_NAME_COLUMN));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull User model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
                        + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(),
                ID_COLUMN,
                CREATED_COLUMN,
                LOGIN_COLUMN,
                PASSWORD_HASH_COLUMN,
                ROLE_COLUMN,
                LOCKED_COLUMN,
                EMAIL_COLUMN,
                FIRST_NAME_COLUMN,
                LAST_NAME_COLUMN,
                MIDDLE_NAME_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getLogin());
            statement.setString(4, model.getPasswordHash());
            statement.setString(5, model.getRole().name());
            statement.setBoolean(6, model.isLocked());
            statement.setString(7, model.getEmail());
            statement.setString(8, model.getFirstName());
            statement.setString(9, model.getLastName());
            statement.setString(10, model.getMiddleName());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull User model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? "
                        + "WHERE %s = ?",
                getTableName(),
                LOGIN_COLUMN,
                PASSWORD_HASH_COLUMN,
                ROLE_COLUMN,
                LOCKED_COLUMN,
                EMAIL_COLUMN,
                FIRST_NAME_COLUMN,
                LAST_NAME_COLUMN,
                MIDDLE_NAME_COLUMN,
                ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPasswordHash());
            statement.setString(3, model.getRole().name());
            statement.setBoolean(4, model.isLocked());
            statement.setString(5, model.getEmail());
            statement.setString(6, model.getFirstName());
            statement.setString(7, model.getLastName());
            statement.setString(8, model.getMiddleName());
            statement.setString(9, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        User user = null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                LOGIN_COLUMN
        );
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) user = fetch(rowSet);
            rowSet.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        User user = null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                EMAIL_COLUMN
        );
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) user = fetch(rowSet);
            rowSet.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
