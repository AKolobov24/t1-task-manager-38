package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private final String TABLE_NAME = "tm_session";
    @NotNull
    private final String DATE_COLUMN = "date";
    @NotNull
    private final String ROLE_COLUMN = "role";

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @SneakyThrows
    public void checkDatabaseTable() {
        @NotNull final String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s " +
                        "( " +
                        ID_COLUMN + " character(36) PRIMARY KEY," +
                        CREATED_COLUMN + " timestamp," +
                        USER_ID_COLUMN + " character(36) REFERENCES tm_user (id)," +
                        DATE_COLUMN + " timestamp," +
                        ROLE_COLUMN + " varchar(255)" +
                        ")",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Session fetch(@NotNull ResultSet rowSet) {
        @NotNull Session session = new Session();
        session.setId(rowSet.getString(ID_COLUMN));
        session.setCreated(rowSet.getDate(CREATED_COLUMN));
        session.setRole(Role.toRole(rowSet.getString(ROLE_COLUMN)));
        session.setDate(rowSet.getTimestamp(DATE_COLUMN));
        session.setUserId(rowSet.getString(USER_ID_COLUMN));
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session add(@NotNull Session model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s) "
                        + "VALUES(?, ?, ?, ?, ?)",
                getTableName(),
                ID_COLUMN,
                CREATED_COLUMN,
                ROLE_COLUMN,
                DATE_COLUMN,
                USER_ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            Role role = model.getRole();
            statement.setString(3, role != null ? role.name() : null);
            statement.setTimestamp(4, new Timestamp(model.getDate().getTime()));
            statement.setString(5, model.getUserId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session update(@NotNull Session model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? "
                        + "WHERE %s = ?",
                getTableName(),
                ROLE_COLUMN,
                DATE_COLUMN,
                USER_ID_COLUMN,
                ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            Role role = model.getRole();
            if (role != null) statement.setString(1, role.name());
            statement.setTimestamp(2, new Timestamp(model.getDate().getTime()));
            statement.setString(3, model.getUserId());
            statement.setString(4, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
