package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String TABLE_NAME = "tm_task";
    @NotNull
    private final String NAME_COLUMN = "name";
    @NotNull
    private final String DESCRIPTION_COLUMN = "description";
    @NotNull
    private final String STATUS_COLUMN = "status";
    @NotNull
    private final String PROJECT_ID_COLUMN = "project_id";

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public String getTableName() {
        return TABLE_NAME;
    }

    @SneakyThrows
    public void checkDatabaseTable() {
        @NotNull final String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s " +
                        "( " +
                        ID_COLUMN + " character(36) PRIMARY KEY," +
                        CREATED_COLUMN + " timestamp," +
                        USER_ID_COLUMN + " character(36) REFERENCES tm_user (id)," +
                        NAME_COLUMN + " varchar(255)," +
                        DESCRIPTION_COLUMN + " text," +
                        STATUS_COLUMN + " varchar(255)," +
                        PROJECT_ID_COLUMN + " character(36) REFERENCES tm_project" +
                        ")",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull ResultSet rowSet) {
        @NotNull Task task = new Task();
        task.setId(rowSet.getString(ID_COLUMN));
        task.setCreated(rowSet.getDate(CREATED_COLUMN));
        task.setName(rowSet.getString(NAME_COLUMN));
        task.setDescription(rowSet.getString(DESCRIPTION_COLUMN));
        task.setStatus(Optional
                .ofNullable(Status.toStatus(rowSet.getString(STATUS_COLUMN)))
                .orElse(Status.IN_PROGRESS)
        );
        task.setUserId(rowSet.getString(USER_ID_COLUMN));
        task.setProjectId(rowSet.getString(PROJECT_ID_COLUMN));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull Task model) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) "
                        + "VALUES(?, ?, ?, ?, ?, ?, ? )",
                getTableName(),
                ID_COLUMN,
                CREATED_COLUMN,
                NAME_COLUMN,
                DESCRIPTION_COLUMN,
                STATUS_COLUMN,
                USER_ID_COLUMN,
                PROJECT_ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setTimestamp(2, new Timestamp(model.getCreated().getTime()));
            statement.setString(3, model.getName());
            statement.setString(4, model.getDescription());
            statement.setString(5, model.getStatus().name());
            statement.setString(6, model.getUserId());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull List<Task> taskList = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * from %s WHERE %s = ? AND %s = ?",
                getTableName(),
                USER_ID_COLUMN,
                PROJECT_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) taskList.add(fetch(rowSet));
            rowSet.close();
        }
        return taskList;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull Task model) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? "
                        + "WHERE %s = ?",
                getTableName(),
                NAME_COLUMN,
                DESCRIPTION_COLUMN,
                STATUS_COLUMN,
                USER_ID_COLUMN,
                PROJECT_ID_COLUMN,
                ID_COLUMN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getStatus().name());
            statement.setString(4, model.getUserId());
            statement.setString(5, model.getProjectId());
            statement.setString(6, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

}
