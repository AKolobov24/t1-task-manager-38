package ru.t1.akolobov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof Session) {
            Session anotherSession = (Session) obj;
            return this.getId().equals(anotherSession.getId()) &&
                    this.getUserId().equals(anotherSession.getUserId()) &&
                    this.getRole() == anotherSession.getRole();
        }
        return false;
    }

}
