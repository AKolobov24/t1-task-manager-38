package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IRepository;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    @NotNull
    protected final String ID_COLUMN = "id";
    @NotNull
    protected final String CREATED_COLUMN = "created";

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    public abstract String getTableName();

    public abstract void checkDatabaseTable();

    @NotNull
    protected abstract M fetch(ResultSet rowSet);

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public abstract M update(@NotNull final M model);

    @NotNull
    protected String getSortColumn(@NotNull Sort sort) {
        return sort.getColumnName();
    }

    @Override
    @NotNull
    public Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        @NotNull final PreparedStatement statement = connection.prepareStatement(sql);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull List<M> models = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull PreparedStatement statement = connection.prepareStatement(sql);
             @NotNull ResultSet rowSet = statement.executeQuery()) {
            while (rowSet.next()) models.add(fetch(rowSet));
        }
        return models;
    }


    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final Sort sort) {
        @NotNull List<M> models = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortColumn(sort));
        try (@NotNull PreparedStatement statement = connection.prepareStatement(sql);
             @NotNull ResultSet rowSet = statement.executeQuery()) {
            while (rowSet.next()) models.add(fetch(rowSet));
        }
        return models;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        M model = null;
        @NotNull String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) model = fetch(rowSet);
            rowSet.close();
        }
        return model;
    }


    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        M model = null;
        @NotNull String sql = String.format("SELECT * FROM %s LIMIT 1 OFFSET ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            ResultSet rowSet = statement.executeQuery();
            if (rowSet.next()) model = fetch(rowSet);
            rowSet.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Integer getSize() {
        @NotNull String sql = String.format("SELECT count(*) as size FROM %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql);
             @NotNull final ResultSet rowSet = statement.executeQuery()) {
            if (rowSet.next())
                return rowSet.getInt("size");
            return 0;
        }
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) {
        removeById(model.getId());
        return model;
    }

    @Override
    @SneakyThrows
    public int removeById(@NotNull final String id) {
        @NotNull String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            return statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @NotNull final M model = Optional.ofNullable(findOneByIndex(index))
                .orElseThrow(EntityNotFoundException::new);
        removeById(model.getId());
        return model;
    }

}
